package com.example.weatherapp.networkUtil.provider;

import com.example.weatherapp.models.response.ResponseWeather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherApi {

    @GET("weather")
    Call<ResponseWeather> getWeather(@Query("lat") double lat,
                                    @Query("lon") double lon,
                                    @Query("appid") String apiKey,
                                    @Query("lang") String lang);

}
