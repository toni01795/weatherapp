package com.example.weatherapp;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import com.example.weatherapp.Utils.Constants;
import com.example.weatherapp.databinding.ActivityMainBinding;
import com.example.weatherapp.models.response.ResponseWeather;
import com.example.weatherapp.networkUtil.HttpHelper;
import com.example.weatherapp.networkUtil.provider.WeatherApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    private static final int REQUEST_CODE_LOCATION_PERMISSION = 1001;

    private ActivityResultLauncher<Intent> startActivityIntent = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            (ActivityResult result) -> {
                verificationEnabledLocation();
            }
    );

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.srlRefresh.setOnRefreshListener(() -> verificationEnabledLocation());
        binding.srlRefresh.setRefreshing(true);
        verificationEnabledLocation();
    }

    private void verificationEnabledLocation() {
        binding.srlRefresh.setRefreshing(true);
        if (!this.isLocationEnabled()) {
            Toast.makeText(this,
                    "Tienes deshabilitada la ubicacion en tu dispositivo, por favor enciendela.",
                    Toast.LENGTH_SHORT).show();
            openLocationSettings();
        } else {
            binding.srlRefresh.setRefreshing(false);
            requestLocationPermission();
        }
    }

    private void openLocationSettings() {
        Intent chooserIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityIntent.launch(chooserIntent);
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void getCurrentUbication() {
        binding.srlRefresh.setRefreshing(true);
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            binding.srlRefresh.setRefreshing(false);
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, (Location location) -> {
                if (location != null) {
                    double latitud = location.getLatitude();
                    double longitud = location.getLongitude();
                    callApi(latitud, longitud);
                } else {
                    binding.srlRefresh.setRefreshing(false);
                    Toast.makeText(this,
                            "No tienes ninguna ubicacion guardada de tu google maps.",
                            Toast.LENGTH_LONG).show();
                }
        });
    }

    private void callApi(double lat, double lon) {
        binding.srlRefresh.setRefreshing(true);
        WeatherApi weatherApi = HttpHelper.getInstance().create(WeatherApi.class);
        Call<ResponseWeather> call = weatherApi.getWeather(lat, lon, Constants.API_KEY, Locale.getDefault().getLanguage());
        call.enqueue(new Callback<ResponseWeather>() {
            @Override
            public void onResponse(Call<ResponseWeather> call, Response<ResponseWeather> response) {
                if(!response.isSuccessful()) {
                    return;
                }

                ResponseWeather responseWeather = response.body();
                binding.txtWeatherCondition.setText(responseWeather.getWeather().get(0).getDescription());
                binding.txtDegreePercent.setText(String.format("%.0f", responseWeather.getMain().getTemp() - 273.15).concat("°"));
                binding.txtTempMin.setText(String.format("%.0f", responseWeather.getMain().getTempMin() - 273.15).concat("°"));
                binding.txtSpeadWind.setText(String.format("%.0f", responseWeather.getWind().getSpeed() - 2.237).concat(" mph"));
                binding.txtName.setText(responseWeather.getName());
                binding.txtCountry.setText(responseWeather.getSys().getCountry());
                binding.txtHourSunrise.setText(convertTimestampToDate(responseWeather.getSys().getSunrise(), responseWeather.getTimezone()));
                binding.txtHourSunset.setText(convertTimestampToDate(responseWeather.getSys().getSunset(), responseWeather.getTimezone()));
                binding.srlRefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ResponseWeather> call, Throwable t) {
                System.out.println(t);
                binding.srlRefresh.setRefreshing(false);
            }
        });
    }

    public static String convertTimestampToDate(long timestamp, int timezoneOffsetSeconds) {
        long timestampInMillis = timestamp * 1000;
        Date date = new Date(timestampInMillis + (timezoneOffsetSeconds * 1000));

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        return sdf.format(date);
    }

    @AfterPermissionGranted(REQUEST_CODE_LOCATION_PERMISSION)
    private void requestLocationPermission() {
        String[] perms = { Manifest.permission.ACCESS_FINE_LOCATION };
        if (EasyPermissions.hasPermissions(this, perms)) {
            getCurrentUbication();
        } else {
            EasyPermissions.requestPermissions(this, "Se requiere permiso de ubicación para obtener la ubicación actual.",
                    REQUEST_CODE_LOCATION_PERMISSION, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION) {
            getCurrentUbication();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            requestLocationPermission();
            new AppSettingsDialog.Builder(this)
                    .setTitle(R.string.title_required_permission)
                    .setRationale(R.string.mesage_required_permission)
                    .build().show();
        }
    }
}