package com.example.weatherapp.models;

import com.google.gson.annotations.SerializedName;

public class CoordinateModel {
    @SerializedName("lat")
    private double latitud;
    @SerializedName("lon")
    private double longitude;

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
