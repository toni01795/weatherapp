package com.example.weatherapp.models;

import com.google.gson.annotations.SerializedName;

public class WindModel {
    @SerializedName("speed")
    private float speed;
    @SerializedName("deg")
    private float deg;
    @SerializedName("gust")
    private float gust;

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getDeg() {
        return deg;
    }

    public void setDeg(float deg) {
        this.deg = deg;
    }

    public float getGust() {
        return gust;
    }

    public void setGust(float gust) {
        this.gust = gust;
    }
}
