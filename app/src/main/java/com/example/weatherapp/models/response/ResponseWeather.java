package com.example.weatherapp.models.response;

import com.example.weatherapp.models.CoordinateModel;
import com.example.weatherapp.models.MainModel;
import com.example.weatherapp.models.SysModel;
import com.example.weatherapp.models.WeatherModel;
import com.example.weatherapp.models.WindModel;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;

public class ResponseWeather {

    @SerializedName("id")
    private long id;
    @SerializedName("base")
    private String base;
    @SerializedName("visibility")
    private int visibility;
    @SerializedName("dt")
    private long dt;
    @SerializedName("timezone")
    private int timezone;
    @SerializedName("name")
    private String name;
    @SerializedName("cod")
    private String cod;
    @SerializedName("coord")
    private CoordinateModel coordinate;
    @SerializedName("weather")
    private List<WeatherModel> weather;
    @SerializedName("main")
    private MainModel main;
    @SerializedName("wind")
    private WindModel wind;
    @SerializedName("rain")
    private HashMap<String, Float> map;
    @SerializedName("clouds")
    private HashMap<String, Float> clouds;
    @SerializedName("sys")
     private SysModel sys;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public int getTimezone() {
        return timezone;
    }

    public void setTimezone(int timezone) {
        this.timezone = timezone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public CoordinateModel getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(CoordinateModel coordinate) {
        this.coordinate = coordinate;
    }

    public List<WeatherModel> getWeather() {
        return weather;
    }

    public void setWeather(List<WeatherModel> weather) {
        this.weather = weather;
    }

    public MainModel getMain() {
        return main;
    }

    public void setMain(MainModel main) {
        this.main = main;
    }

    public WindModel getWind() {
        return wind;
    }

    public void setWind(WindModel wind) {
        this.wind = wind;
    }

    public HashMap<String, Float> getMap() {
        return map;
    }

    public void setMap(HashMap<String, Float> map) {
        this.map = map;
    }

    public HashMap<String, Float> getClouds() {
        return clouds;
    }

    public void setClouds(HashMap<String, Float> clouds) {
        this.clouds = clouds;
    }

    public SysModel getSys() {
        return sys;
    }

    public void setSys(SysModel sys) {
        this.sys = sys;
    }
}
